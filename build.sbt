import java.io.File

import _root_.sbt.Keys._

classpathTypes += "maven-plugin"

lazy val openCVBinPath: String = {
  sys.props("os.name") match {
    case "Mac OS X" => "/Users/claas/git/opencv/build/bin"
    case "Linux" => "/home/sklemm/opencv/build/bin"
    case _ => "C:\\opencv\\build\\java"
  }
}

lazy val commonSettings = Seq(
  organization := "de.wwu",
  version := "0.1.0",
  scalaVersion := "2.10+",
  scalacOptions ++= Seq(
    "-optimize",
    "-unchecked",
    "-deprecation" //for implicit conversions
  )
)

libraryDependencies := {
  libraryDependencies.value ++ Seq(
    "org.scala-lang" % "scala-swing" % scalaVersion.value,
    "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
    "org.apache.commons" % "commons-math3" % "3.0",
    "net.sourceforge.jmatio" % "jmatio" % "1.0"
  )
}

unmanagedJars in Compile ++= {
  val base = baseDirectory.value
  val opencvDir:File = {
    sys.props("os.name") match {
      case "Mac OS X" => new File("/Users/claas/git/opencv/build/bin")
      case "Linux" => new File("/home/sklemm/opencv/build/bin")
      case _ => new File("C:\\opencv\\build\\java")
    }
  }
  val baseDirectories = opencvDir
  val customJars = (baseDirectories ** "*.jar")
  customJars.classpath
}

fork := true

lazy val openCVLibPath: String = {
  sys.props("os.name") match {
    case "Mac OS X" => "/Users/claas/git/opencv/build/lib"
    case "Linux" => "/home/sklemm/opencv/build/lib"
    case _ => "C:\\opencv\\build\\java\\x64"
  }
}

javaOptions += "-Djava.library.path=" + openCVLibPath


