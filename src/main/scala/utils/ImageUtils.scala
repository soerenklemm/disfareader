package utils

import java.awt.image.{BufferedImage, DataBufferByte}

import org.opencv.core.{Core, CvType, Mat, Point, Size}
import org.opencv.highgui.Highgui
import org.opencv.imgproc.Imgproc

import scala.reflect.ClassTag

/**
 * Created by Claas Kähler on 22.07.15.
 */
class ImageUtils {

  implicit def intToSize(x: Int) : Size = new Size(x, x)  //allows passing a single Integer to a Size parameter

  /**
   * Fastest method i could found for converting Mat to a Buffered Image.
   * See http://cell0907.blogspot.com.es/2013/12/from-mat-to-bufferedimage.html
   *
   * @param matBGR An OpenCV image
   * @return A BufferedImage useable for SWING
   */
  def MatToBufferedImage(matBGR: Mat): BufferedImage = {
    val width: Int = matBGR.width()
    val height: Int = matBGR.height()
    val channels: Int = matBGR.channels()
    val sourcePixels: Array[Byte] = new Array[Byte](width * height * channels)
    matBGR.get(0, 0, sourcePixels)
    // create new image and get reference to backing data
    val image: BufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR)
    val targetPixels: Array[Byte] = image.getRaster().getDataBuffer().asInstanceOf[DataBufferByte].getData
    System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length)
    image
  }



  def createComGabor(ksize: Size, sigma: Double = 0d, theta: Double = 0d, lambda: Double = 0d, gamma: Double = 1d, psi: Double = 0d, ktype: Int = CvType.CV_64F): (Mat, Mat) = {
    //set default values, check for correct input
    val _sigma = if (sigma <= 0) scala.math.max(ksize.height, ksize.width) / 6d else sigma / 2
    val _theta = theta / 360d * 2 * scala.math.Pi
    val _lambda = if (lambda <= 0) _sigma * 2 else lambda
    val _gamma = if (gamma <= 0) 1d else gamma
    val _psi = psi

    val sigma_x = _sigma
    val sigma_y = _sigma / _gamma
    val nstds = 3
    val c = scala.math.cos(_theta)
    val s = scala.math.sin(_theta)

    val xmax: Int = {
      if (ksize.width > 0)
        (ksize.width / 2).toInt
      else
        scala.math.max(scala.math.abs(nstds * sigma_x * c), scala.math.abs(nstds * sigma_y * s)).round.toInt
    }

    val ymax: Int = {
      if (ksize.height > 0)
        (ksize.height / 2).toInt
      else
        scala.math.max(scala.math.abs(nstds * sigma_x * s), scala.math.abs(nstds * sigma_y * c)).round.toInt
    }

    val xmin = -xmax
    val ymin = -ymax

    assert(ktype == CvType.CV_32F || ktype == CvType.CV_64F)

    val reKernel = new Mat(ymax - ymin + 1, xmax - xmin + 1, ktype)
    val imKernel = new Mat(ymax - ymin + 1, xmax - xmin + 1, ktype)

    val scale: Double = 1d
    val ex: Double = -0.5 / (sigma_x * sigma_x)
    val ey: Double = -0.5 / (sigma_y * sigma_y)
    val cscale: Double = scala.math.Pi * 2d / _lambda

    for (y <- ymin to ymax)
      for (x <- xmin to xmax) {
        val xr = x * c + y * s
        val yr = -x * s + y * c

        val magn = scale * scala.math.exp(ex * xr * xr + ey * yr * yr)
        val rv = magn * scala.math.cos(cscale * xr + _psi)
        val iv = magn * scala.math.sin(cscale * xr + _psi)


        if (ktype == CvType.CV_32F) {
          reKernel.put(ymax - y, xmax - x, rv.toFloat)
          imKernel.put(ymax - y, xmax - x, iv.toFloat)
        } else {
          reKernel.put(ymax - y, xmax - x, rv)
          imKernel.put(ymax - y, xmax - x, iv)
        }
      }

    (reKernel, imKernel)
  }

  def complexFilter(image: Mat, kernels: (Mat, Mat)): (Mat, Mat) = {
    val realKernel = new Mat(kernels._2.size(), kernels._2.`type`())
    val imagKernel = new Mat(kernels._2.size(), kernels._2.`type`())
    Core.flip(kernels._1, realKernel, -1) //to achieve real convolution
    Core.flip(kernels._2, imagKernel, -1) //dito

    val realFiltered = new Mat()
    val imagFiltered = new Mat()

    Imgproc.filter2D(image, realFiltered, -1, realKernel, new Point(-1, -1), 0, Imgproc.BORDER_REFLECT_101)
    Imgproc.filter2D(image, imagFiltered, -1, imagKernel, new Point(-1, -1), 0, Imgproc.BORDER_REFLECT_101)

    (realFiltered, imagFiltered)
  }

  def magn(image: (Mat, Mat)): Mat = {
    val result = new Mat(image._1.size(), CvType.CV_64F)
    for (row <- 0 to (image._1.rows() - 1))
      for (col <- 0 to (image._1.cols() - 1)) {
        val re = image._1.get(row, col)(0)
        val im = image._2.get(row, col)(0)
        result.put(row, col, scala.math.sqrt(re * re + im * im))
      }
    result
  }

  def phase(image: (Mat, Mat)): Mat = {
    val result = new Mat(image._1.size(), CvType.CV_64F)
    for (row <- 0 to (image._1.rows() - 1))
      for (col <- 0 to (image._1.cols() - 1)) {
        val re = image._1.get(row, col)(0)
        val im = image._2.get(row, col)(0)
        result.put(row, col, scala.math.atan2(re, im))
      }
    result
  }

  def maPh(image: (Mat, Mat)): (Mat, Mat) = {
    val magn = new Mat(image._1.size(), CvType.CV_64F)
    val phase = new Mat(image._1.size(), CvType.CV_64F)
    for (row <- 0 to (image._1.rows() - 1))
      for (col <- 0 to (image._1.cols() - 1)) {
        val re = image._1.get(row, col)(0)
        val im = image._2.get(row, col)(0)
        magn.put(row, col, scala.math.sqrt(re * re + im * im))
        phase.put(row, col, scala.math.atan2(re, im))
      }
    (magn, phase)
  }

  def saveToFile[T: ClassTag](images : List[T], f: (T) => Mat, filename : String, extension : String) = {
    var index = 0
    for (image <- images)
      {

        assert(Highgui.imwrite(filename+"_"+index+extension,openCVTools.writeMat(f(image))))
        index += 1
      }
  }

  def saveToFile[T: ClassTag](images : List[T], fs: List[(T) => Mat], filename : String, extension : String) = {
    var index = 0
    for (image <- images)
      for (f <- fs){

      assert(Highgui.imwrite(filename+"_"+index+extension,openCVTools.writeMat(f(image))))
      index += 1
    }
  }

}
