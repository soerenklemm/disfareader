package utils

import org.opencv.core._

/**
 * Created by sklemm on 11.08.2015.
 */

object openCVTools {

  def diff(mat1 : Mat, mat2 : Mat) : Mat = {
     if (mat1.`type`() != mat2.`type`()) {
       println("Matrix types do not match!")
       throw new IllegalArgumentException
     }

    if (mat1.size() != mat2.size()){
      println("Matrix sizes do not match!")
      throw new IllegalArgumentException
    }

    val result = new Mat(mat1.size(),CvType.CV_32F)

    for (row <- 0 to mat1.rows()-1)
      for (col <- 0 to mat1.cols()-1){
        val v1 = mat1.get(row,col)(0)
        val v2 = mat2.get(row,col)(0)

        result.put(row,col,v1-v2)
      }
    result
  }

  def printMat(mat : Mat) =  {
    for (row <- 0 to mat.rows()-1) {
      for (col <- 0 to mat.cols() - 1)
        if (mat.`type`() == CvType.CV_32F || mat.`type`() == CvType.CV_32F)
          print("%5.2f ".format(mat.get(row,col)(0)))
      else
          print("%3.0f ".format(mat.get(row,col)(0)))

      println()
    }
  }

  def writeMat(mat: Mat) : Mat = {

    val result = new Mat(mat.size(),CvType.CV_8UC1)

    //normalize image
    var max : Float= mat.get(0,0)(0).toFloat
    var min : Float= mat.get(0,0)(0).toFloat

    for (row <- 0 to mat.rows()-1)
      for (col <- 0 to mat.cols() - 1) {
        val v = mat.get(row, col)(0).toFloat
        max = scala.math.max(max, v)
        min = scala.math.min(min, v)
      }
    for (row <- 0 to mat.rows()-1)
        for (col <- 0 to mat.cols() - 1){
          result.put(row,col,(((mat.get(row,col)(0).toFloat - min)  / (max-min))*255f).toInt)
        }

    result
  }

  def drawPoints(mat: Mat, points: List[Point], color: Scalar = new Scalar(0,255,0)) = {
    for (pt <- points) Core.circle(mat, pt, 3,color)
  }

  def drawRects(mat: Mat, rects: MatOfRect, color: Scalar = new Scalar(0,255,0)) = {
    val _rects = rects.toArray
    for (rect <- _rects){
      Core.rectangle (mat, rect.tl,rect.br,color)
    }
  }
}
