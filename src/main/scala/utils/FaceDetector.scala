package utils

import java.io.File

import org.opencv.core._
import org.opencv.objdetect.CascadeClassifier

/**
 * Created by Claas Kähler on 22.07.15.
 */
class FaceDetector {
  // Create a face detector from the cascade file in the resources directory.
  val classLoader = getClass().getClassLoader()
  val cascadeFile = new File(classLoader.getResource("lbpcascade_frontalface.xml").getPath)
  val faceDetector = new CascadeClassifier(cascadeFile.getAbsolutePath)

  // Holds the regions of detected faces
  var faceDetections = new MatOfRect()

  var faces: List[Face] = List()

  /**
   * Detect faces in the given image
   *
   * @param image
   */
  def find(image: Mat): Rect = {

    val newFaceDetections = new MatOfRect
    var newDetections = Array[Rect]()
    val scale = 1.1

    if (faces.length == 0) {
      faceDetector.detectMultiScale(image, newFaceDetections)
      newDetections = newFaceDetections.toArray
    }
    else {
      val minSize = faces.head.getRegion().width / (scale)
      val maxSize = faces.head.getRegion().width * (scale)
      val min_x = scala.math.max(0, faces.head.getRegion().tl().x -  faces.head.getRegion().width/2)
      val max_x = scala.math.min(faces.head.getRegion().br().x +  faces.head.getRegion().width/2, image.width())
      val min_y = scala.math.max(0, faces.head.getRegion().tl().y - faces.head.getRegion().height/2)
      val max_y = scala.math.min(faces.head.getRegion().br().y +  faces.head.getRegion().height/2, image.height())
      val ROI = new Mat(image, new Rect(new Point(min_x, min_y), new Point(max_x, max_y)))
      faceDetector.detectMultiScale(ROI, newFaceDetections, scale, 3, 0, new Size (minSize, minSize), new Size (maxSize, maxSize))
      newDetections = newFaceDetections.toArray
      for (idx <- newDetections.indices)
        newDetections(idx) =  new Rect(new Point(newDetections(idx).tl.x+min_x,newDetections(idx).tl.y+min_y), newDetections(idx).size())
    }

    for (face <- faces) {
      face.foundAgain = false
    }

    // iterate through all founded faces in that image
    for (rect <- newDetections) {
      if (faces.isEmpty) {
        val face = new Face()
        face.addRegion(rect)
        faces :::= List(face)
      } else {
        // Checke weather the face was found before
        var rectFound = false
        for (face <- faces) {
          if (face.faceNearRegion(rect)) {
            face.addRegion(rect)
            if (face.counts < 10) {
              face.counts += 1
            }
            face.foundAgain = true
            rectFound = true;
          }
        }
        // If it was not found before: add it!
        if (!rectFound) {
          val face = new Face();
          face.addRegion(rect)
          faces :::= List(face)
        }
      }
    }

    // Lower count for faces that weren't found
    for (face <- faces; if !face.foundAgain) {
      face.counts -= 1
    }

    // Kick out Faces that weren't found for too long
    var newFaces: List[Face] = List()
    for (face <- faces; if face.counts > -5)
        newFaces :::= List(face)

    faces = newFaces
    if (faces.length > 0)
     faces.head.getRegion()
    else
      new Rect(0,0,image.width, image.height)
  }

  /**
   * Draws bounding boxes for faces
   * @param image the image where you want to draw the bounding boxes
   * @param faces the bounding boxes
   */

  def drawBoundingBoxesForFaces(image: Mat, faces: MatOfRect): Unit = {
    // Draw a bounding box around each face.
    var i: Int = 1
    for (rect <- faces.toArray) {
      Core.rectangle(
        image,
        new Point(rect.x, rect.y + 5),
        new Point(rect.x + rect.width,
          rect.y + rect.height),
        new Scalar(0, 255, 0))
      Core.putText(image, "Face " + i, new Point(rect.x, rect.y), 0, 0.5, new Scalar(255, 0, 0))
      i += 1
    }
  }

  def drawBoundingBoxesForFace(face: Face, image: Mat): Unit = {
    val rect = face.getRegion()
    Core.rectangle(
      image,
      new Point(rect.x, rect.y + 5),
      new Point(rect.x + rect.width,
        rect.y + rect.height),
      new Scalar(0, 255, 0))
    Core.putText(image, "Face " + face.id, new Point(rect.x, rect.y), 0, 0.5, new Scalar(255, 0, 0))
  }

}


