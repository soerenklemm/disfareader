package utils

import java.io.{FileNotFoundException, File}

import com.jmatio.io.MatFileReader
import com.jmatio.types.MLDouble
import org.opencv.core.Point

import scala.collection.mutable.ListBuffer

/**
 * Created by klemms on 14.08.2015.
 */
object fileSystem {

  def getListOfSubDirectories(directoryName: String): Array[String] = {
    new File(directoryName).listFiles.filter(_.isDirectory).map(_.getName)
  }

  def getListOfFiles(directoryName: String): Array[String] = {
    new File(directoryName).listFiles.filter(_.isFile).map(_.getName)
  }

  def getMatFile(filename: String) : List[Point]= {
  var result = List[Point]()
    try {
      val matFileReader = new MatFileReader(filename)
      val keys = matFileReader.getContent.keySet().toArray
      val data = matFileReader.getMLArray(keys(0).asInstanceOf[String]).asInstanceOf[MLDouble].getArray
      val points = new ListBuffer[Point]()
      for (x <- data) points += new Point(x(0), x(1))
      result = points.toList
    } catch {
      case e: FileNotFoundException => println("File " + filename + " not found.")
      case e: Throwable => println(e)
    }
    result
  }

}
