package utils

import org.opencv.core.Rect

import scala.util.Random

/**
 * Created by Claas Kähler on 24.07.15.
 */
class Face {
  val maxRegions: Int = 10
  var regions: Array[Rect] = new Array[Rect](maxRegions)
  var regionPointer: Int = 0
  var counts: Int = 0
  var foundAgain: Boolean = true
  val id: Int = Random.nextInt(100)


  /**
   * Indicates if the region of this Face is simular to the given one.
   * @return similar or not.
   */
  def faceNearRegion(rect: Rect): Boolean ={
    var similar = true
    val faceRegion = getRegion()
    val variance_x =  (0.2 * faceRegion.width).toInt
    val variance_y =  (0.2 * faceRegion.height).toInt

    if (rect.x < faceRegion.x - variance_x){
      similar = false
    } else if (rect.x + rect.width > faceRegion.x + faceRegion.width + variance_x) {
      similar = false
    } else if (rect.y < faceRegion.y - variance_y) {
      similar = false
    } else if (rect.y + rect.height > faceRegion.y + faceRegion.height + variance_y) {
      similar = false
    }
    similar
  }


  /**
   * Add a subregion to the face. If the maximum of regions are reached, it will overwrite the oldest region
   * @param rect
   */
  def addRegion(rect: Rect): Unit ={
    if (regionPointer  >= maxRegions -1){
      regionPointer = 0
    } else {
      regionPointer += 1
    }
    regions(regionPointer) = rect
  }

  /**
   * The minimum Region that contains all sub-regions of this Face
   * @return a region
   */
  def getRegion(): Rect = {
    var min_x: Int = Int.MaxValue
    var min_y: Int = Int.MaxValue
    var max_x: Int = Int.MinValue
    var max_y: Int = Int.MinValue

    // find the Borders of all regions
    for(region <- regions){
      if (region != null) {
        if (region.x < min_x) {
          min_x = region.x
        }
        if (region.x + region.width > max_x) {
          max_x = region.x + region.width
        }
        if (region.y < min_y) {
          min_y = region.y
        }
        if (region.y + region.height > max_y) {
          max_y = region.y + region.height
        }
      }
    }

    new Rect(min_x, min_y, max_x - min_x, max_y-min_y)
  }
}
