package utils

import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc

/**
 * Created by klemms on 18.08.2015.
 */
class LDB(image: Mat) {
  private val grayImage = {
    if (image.channels() == 1)
      image.clone()
    else {
      val grayImage = new Mat()
      Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_BGR2GRAY)
      grayImage
    }
  }

  private val integralImage = new IntegralImage(grayImage)

  private def f_dx(x_0: Int, y_0: Int, x_1: Int, y_1: Int): Int = {
    if (x_1 < x_0 | y_1 < y_0) 0
    else {
      if (((x_1 - x_0) % 2) == 0)
        f_i((x_0 + x_1) / 2, y_0, x_1, y_1) - f_i(x_0, y_0, (x_0 + x_1) / 2, y_1)
      else
        f_i((x_0 + x_1) / 2 + 1, y_0, x_1, y_1) - f_i(x_0, y_0, (x_0 + x_1) / 2, y_1)
    }
  }

  private def f_dy(x_0: Int, y_0: Int, x_1: Int, y_1: Int): Int = {
    if (x_1 < x_0 | y_1 < y_0) 0
    else {
      if (((y_1 - y_0) % 2) == 0)
        f_i(x_0, (y_0 + y_1) / 2, x_1, y_1) - f_i(x_0, y_0, x_1, (y_0 + y_1) / 2)
      else
        f_i(x_0, (y_0 + y_1) / 2 + 1, x_1, y_1) - f_i(x_0, y_0, x_1, (y_0 + y_1) / 2)
    }
  }

  /**Returns the sum of intensity values for the rectangle defined by upper left corner (x_0, y_0)
   * and lower right corner (x_1, y_1).
   */
  private def f_i(x_0: Int, y_0: Int, x_1: Int, y_1: Int): Int = {
    integralImage.get(x_1, y_1) - integralImage.get(x_0, y_0)
  }
}
