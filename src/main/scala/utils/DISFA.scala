package utils

import java.io.File

import org.opencv.core._
import org.opencv.highgui.VideoCapture
import org.opencv.imgproc.Imgproc

/**
 * Created by klemms on 18.08.2015.
 */
class DISFA(sessionNumber: Int) {

  private val DISFAFolder = "T:\\DISFA\\"
  private val rightVideosFolder = DISFAFolder + "Video_RightCamera\\"
  private val leftVideosFolder = DISFAFolder + "Videos_LeftCamera\\"
  private val sessionPattern = "SN" + f"$sessionNumber%03d"
  private val landmarksFolder = {
    if (new File(DISFAFolder + "Landmark_Points\\"+sessionPattern+"\\frame_lm\\").isDirectory)
      DISFAFolder + "Landmark_Points\\"+sessionPattern+"\\frame_lm\\"
    else
      DISFAFolder + "Landmark_Points\\"+sessionPattern+"\\tmp_frame_lm\\"
  }
  private val landmarksFile = {
    if (new File(landmarksFolder+sessionPattern+"_0000_lm.mat").isFile)
    sessionPattern+"_"
    else
    "l0"
  }
  private val leftFaceDetector = new FaceDetector
  private val rightFaceDetector = new FaceDetector
  private var leftVideo: VideoCapture = _
  private var rightVideo: VideoCapture = _
  private var leftFrameCounter = { if (landmarksFile == "l0") 0 else -1}
  private var rightFrameCounter = leftFrameCounter
  private var leftRegion = new Rect(0, 0, 0, 0)
  private var rightRegion = new Rect(0, 0, 0, 0)

  openVideos


  def getFrames(faceOnly: Boolean = false, size : Size = new Size(0,0)): (Mat, Mat, List[Point]) = {
    val lFrame = {
      while (leftFrameCounter < rightFrameCounter) nextLeftFrame(false,size)
      nextLeftFrame(faceOnly, size)
    }
    val rFrame = {
      while (rightFrameCounter < leftFrameCounter - 1) nextRightFrame(false, size)
      nextRightFrame(faceOnly, size)
    }

    var landmarks = getLandmarks

    if (leftRegion.tl != new Point(0, 0)) {
      if (size == new Size(0, 0))
        landmarks = landmarks.map(pt => new Point(pt.x - leftRegion.tl.x, pt.y - leftRegion.tl.y))
      else
        landmarks = landmarks.map(pt => new Point((pt.x - leftRegion.tl.x)/leftRegion.width*size.width, (pt.y - leftRegion.tl.y)/leftRegion.height*size.height))
    }
    (lFrame, rFrame, landmarks)
  }

  def getLandmarks: List[Point] = {
    utils.fileSystem.getMatFile(landmarksFolder + landmarksFile + f"$leftFrameCounter%04d" + "_lm.mat")
  }

  def nextLeftFrame(faceOnly: Boolean, size : Size): Mat = {
    var frame = new Mat()
    if (leftVideo.isOpened) {
      if (leftVideo.read(frame)) {
        leftFrameCounter += 1
        if (faceOnly) {
          leftRegion = leftFaceDetector.find(frame)
          frame = new Mat(frame, leftRegion)
        }
        else {
          leftRegion = new Rect(0, 0, frame.width(), frame.height())
        }
        if (size != new Size(0,0)) Imgproc.resize(frame,frame, size)
      }
    }
    frame
  }

  def nextRightFrame(faceOnly: Boolean, size : Size): Mat = {
    var frame = new Mat()
    if (rightVideo.isOpened) {
      if (rightVideo.read(frame)) {
        rightFrameCounter += 1
        if (faceOnly) {
          rightRegion = rightFaceDetector.find(frame)
          frame = new Mat(frame, rightRegion)
        }
        else {
          rightRegion = new Rect(0, 0, frame.width(), frame.height())
        }
        if (size != new Size(0,0)) Imgproc.resize(frame,frame, size)
      }
    }
    frame
  }

  def getLeftRegion: Rect = leftRegion

  def getRightRegion: Rect = rightRegion

  private def openVideos: Boolean = {
    leftVideo = new VideoCapture(leftVideosFolder + "LeftVideo" + sessionPattern + "_comp.avi")
    rightVideo = new VideoCapture(rightVideosFolder + "RightVideo" + sessionPattern + "_comp.avi")
    val lOpen = leftVideo.isOpened
    val rOpen = rightVideo.isOpened
    val landmarksExist = new File(landmarksFolder + landmarksFile + "0001" + "_lm.mat").exists

    if (!lOpen) println("File: " + leftVideosFolder + "LeftVideo" + sessionPattern + "_comp.avi" + " not found.")
    if (!rOpen) println("File: " + rightVideosFolder + "RightVideo" + sessionPattern + "_comp.avi" + " not found.")
    if (!landmarksExist) println("File: " + landmarksFolder + landmarksFile + "0001" + "_lm.mat"+ " not found.")
    lOpen & rOpen & landmarksExist
  }

}
