package utils

import org.opencv.core.Mat

/**
 * Created by klemms on 18.08.2015.
 */
class IntegralImage(image: Mat) {
  val rowSize = image.cols
  val colSize = image.rows
  var values: List[Int] = initialise

  def get(x: Int, y: Int): Int = {
    assert(x >= 0 & y >= 0 & x < rowSize & y < colSize)
    values(y * rowSize + x)
  }

  private def initialise: List[Int] = {
    val values = Array.fill(image.rows * image.cols)(0)
    values(0) = image.get(0, 0)(0).toInt

    for (col <- 1 to image.cols - 1) {
      values(col) = values(col - 1) + image.get(0, col)(0).toInt
    }

    for (row <- 1 to image.rows - 1) {
      var thisRow = 0
      for (col <- 0 to image.cols - 1) {
        thisRow += image.get(row, col)(0).toInt
        values(row * rowSize + col) = values((row - 1) * rowSize + col) + thisRow
      }
    }
    values.toList
  }

}
