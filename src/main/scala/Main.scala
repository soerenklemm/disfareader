import java.awt.{BorderLayout, Dimension, Color}
import java.io.File
import javax.swing.JFrame

import com.sun.imageio.plugins.common.ImageUtil
import org.opencv.core._
import org.opencv.highgui.VideoCapture
import org.opencv.imgproc.Imgproc
import utils.{DISFA, FaceDetector, ImageUtils}
import view.CamPanel




/**
 * Created by klemms on 14.08.2015.
 */
object Main {

  System.loadLibrary(Core.NATIVE_LIBRARY_NAME)



  def main(args: Array[String]) {

    val imgUtl = new ImageUtils

    val leftDisplay = new CamPanel
    val rightDisplay = new CamPanel
    val framePanel :JFrame = new JFrame("BasicPanel")
    framePanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    leftDisplay.setBackground(Color.CYAN)
    leftDisplay.setPreferredSize(new Dimension(256,256))
    rightDisplay.setBackground(Color.CYAN)
    rightDisplay.setPreferredSize(new Dimension(256,256))
    framePanel.getContentPane.add(leftDisplay, BorderLayout.WEST)
    framePanel.getContentPane.add(rightDisplay, BorderLayout.EAST)
    framePanel.pack()
    framePanel.setVisible(true)

    val session = 16

    val frameSource = new DISFA(session)

    var timeStamp = System.currentTimeMillis()

    var goOn = true

    while (goOn){
      val (lFrame, rFrame, landmarks) = frameSource.getFrames(true, new Size(256,256))
      if (lFrame.width() > 0){
      utils.openCVTools.drawPoints(lFrame,landmarks)
      val delta = 1000.0/(System.currentTimeMillis() - timeStamp)
      timeStamp = System.currentTimeMillis()
      val str = f"$delta%2.2f"
      Core.putText(lFrame,str + "fps",new Point(50,50),1,1,new Scalar (0,255,0))
      leftDisplay.image = imgUtl.MatToBufferedImage(lFrame)
      leftDisplay.repaint()
      //rightDisplay.image = imgUtl.MatToBufferedImage(rFrame)
      //rightDisplay.repaint()
      }
      else
        goOn = false


    }

    framePanel.dispose()

    println("finished")
  }

}
