package view

import java.awt.Graphics
import java.awt.image.BufferedImage
import javax.swing.JPanel


/**
 * Created by Claas Kähler on 22.07.15.
 */
class CamPanel extends JPanel () {
  val serialVersionUID: Long = 1L
  var image: BufferedImage = _


  override def paintComponent(g: Graphics): Unit = {
    if (image != null) {
      val temp: BufferedImage = this.image
      g.drawImage(temp, 0, 0, temp.getWidth(), temp.getHeight(), this)
    }
  }
}


